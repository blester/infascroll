$(document).ready(function() {

    ds = datasource();

    var infa = infascroll({
        container : '#infactr',
        getMoreItems : function(from, onReady) {
            var lmt = 50, subset = [], haveMore = true;
            for(var i=0; i < lmt; i++) {
                if(!ds[from+i]) { haveMore = false; break }
                subset.push(ds[from+i]);
            }
            window.setTimeout(function() {
                onReady(subset,  haveMore);
            }, Math.random()*1000);
        },
        itemContainer : function(data, index, pos) {
            var item = data[index];
            return $('<div/>').append(item.s).css({
                backgroundColor : '#eee',
                border : '1px solid #ccc',
                color : '#fff',
                fontSize : '42px',
                position : 'relative',
                float : 'left',
                padding : '7px',
                fontWeight : 'bold',
                margin : 0
            }).append(
                $('<div/>').css({
                    position : 'absolute',
                    color : '#fff',
                    fontSize : '10px',
                    top : 2,
                    left : 2
                }).text(pos)
            );
        }
    }).init();


    function datasource() {

        var records = 1000, i, data = [];

        for(i=0; i < records; i++) {
            data.push({
                id : i+1,
                s  : randString(4)
            });
        }

        datasource = function() {
            return data;
        };

        return data;

    }

    function randString(len) {
        len = len || 10;
        var s = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789_$',
            out = '',
            randomIndex;
        for(var i=0; i < len; i++) {
            randomIndex = Math.round(Math.random()*(s.length-1));
            out += s.substring(randomIndex, randomIndex+1)
        }
        return out
    }

});