/**
 * Infascroll, an infinite scroll API by Bret Lester
 */
var infascroll = (function() {

    var debugmode = false;

    return function(conf){
        var
            /**
             * The DOM element that will contain the data.
             *
             * Infascroll assumes the container has a fixed width and height and has css
             * position:relative and overflow:auto
             */
            container = typeof(conf.container) == 'string' ? $(conf.container) : conf.container,

            /**
             * Client-Defined Function responsible for getting more data when Infascroll asks for it. The quantity of
             * data returned, and how it is retrieved is arbitrary and relies upon the client's judgement to determine the best approach.
             * For efficiency-sake it is probably a good idea to return enough data to fill the available real estate within the
             * container element without making multiple requests.
             *
             * The getMoreItems function is called with two arguments:
             *   (1) currentIndex (Number) - The positional index from which to start retrieving data
             *   (2) onReady (Function)  - Function to be called when data is available (See below).
             *
             * The onReady callback requires two arguments:
             *   (1) data (Array) - An array of data items representing the data
             *   (2) haveMore (Boolean) - Indicates whether more data is available beyond what was just provided
             */
            getMoreItems = conf.getMoreItems,

            /**
             * Client-Defined Function responsible for creating visual representations of data items.
             *
             * It is assumed that each data element has the same width and height!
             *
             * The itemContainer function is called with 3 arguments:
             *   (1) data (Array) - The entire array of data that was provided to the onReady callback described above.
             *   (2) index (Number) - The index of the data object within the data array that is currently being processed
             *   (3) position (Number) - The overall positional index
             *
             * Returns a jquery/DOM object representing the specified data item. The returned object will be appended
             * to the container and automatically positioned by Infascroll.
             */
            itemContainer = conf.itemContainer,

            /**
             * (Number) horizontal/vertical spacing between data items
             */
            cellSpacing = typeof(conf.cellSpacing) == 'undefined' ?  2 : conf.cellSpacing,

            holder = $('<div style="position:absolute"> </div>');

        container.append(holder);

        return {
            init : function() {
                var currentIndex = 0, item, iteration = 0, left, top, row = 0, cellSpacing = 2;

                getMore();

                function getMore() {

                    debugmode && console.log('get more! position='+currentIndex);

                    getMoreItems(currentIndex, function(data, haveMore) {

                        debugmode && console.log('received '+data.length+' records ');

                        var paddingLeft = parseInt(container.css('padding-left')),
                            paddingTop = parseInt(container.css('padding-top')),
                            paddingBottom = parseInt(container.css('padding-bottom')),
                            maxScrollTop, currentHeight, lastTop;

                        for(var i=0; i < data.length; i++) {

                            currentIndex++;

                            left = typeof(left) == 'undefined' ? paddingLeft : left;
                            top = typeof(top) == 'undefined' ? paddingTop : top;

                            item = itemContainer(data, i, currentIndex-1).css({
                                position : 'absolute',
                                left : left,
                                top : top
                            });
                            container.append(item);

                            if(lastTop != top) {
                                // must be a new row
                                holder.height(item.outerHeight()*(row+1)+cellSpacing*row+paddingBottom);
                            }
                            lastTop = top;

                            if(item.position().left + item.outerWidth()*2 > container.width() + paddingLeft) {

                                // new row predicted

                                left = paddingLeft;
                                top = (item.position().top + container.scrollTop()) + item.outerHeight() + cellSpacing;

                                row++;
                                currentHeight = (row*item.outerHeight())+(cellSpacing*(row-1));

                                if(currentHeight > (container.height()*2)*(iteration+1)) {
                                    maxScrollTop = (currentHeight+paddingTop) - container.innerHeight() + paddingBottom;
                                    debugmode && console.log('stop! next='+maxScrollTop);
                                    break;
                                }

                            } else {
                                left = item.position().left + item.outerWidth() + cellSpacing;
                                top = item.position().top + container.scrollTop();
                            }

                            //currentIndex = beginningPosition + i;
                        }

                        if((!maxScrollTop) && haveMore) {
                            // not enough data was provided to fill the available area
                            getMore();
                            return
                        }

                        container.unbind('scroll.infa').bind('scroll.infa', function(e) {
                            if(!maxScrollTop) { return }
                            if(container.scrollTop() >= maxScrollTop) {
                                // get more items
                                container.unbind('scroll.infa');
                                iteration++;
                                getMore();
                            }
                        });
                    });
                }
            }
        };
    };

})();

